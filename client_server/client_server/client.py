﻿import socket
import time
import sys
import struct
import numpy as np

#host = socket.gethostname()
host = '10.116.122.67'
#host = '192.168.7.2'
port = 1234               # The same port as used by the server

#create socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#connect to the host
s.connect((host, port))
print('connected')

# start timing how long time it takes to receive and decode data
t = time.time()

# ask the server for data
s.sendall(b'Start sending data')
decoded_data = []
data = b''
# receive and decode data
while True:
    # wait for data
    msg = s.recv(2**16) #2^16

    # have we received the last msg
    if msg == b'end':
        print("send close msg")
        s.sendall(b'close')
        s.close()
        break
    # make binary string to hold all values
    data += msg
  
    #bin_size = sys.getsizeof(msg)
    #print('receiving {0} bytes'.format(bin_size))    
         
    # we are ready to the next message. Ask the server for it
    s.sendall(b'send more')
    

elapsed = time.time() - t

print('elapsed time {0}'.format(elapsed))

print('Decoding data')
end = int(len(data)/4)*4
decoded_data = np.fromstring(data[:end],dtype=np.float32)

print('writing data')

# write the received data to a file for comparison
f = open('recv_data.csv', 'w')
for item in decoded_data:
    f.write('%f\n' % item)
f.close()



