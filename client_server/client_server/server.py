﻿import csv
import sys
import socket
import struct
import numpy as np
import time

data = ''
counter = 0
end = 0

"""
Load file into the global var data 
@param name: the name of the file to load (without the .csv extension)
"""
def open_file(name):
    print("Loading file. Please wait...")
    global data
    data_array = []
    file_name = str(name)+'.csv'
    with open(file_name) as f:
        for line in f:
            data_array.append(np.float32(line))
    # encode data
    print('Encoding data')
    tmp = np.array(data_array,dtype=np.float32)
    data = tmp.tostring()
    print("Encoding done")


"""
Build a message to be send over the socket
@param size: How many number the msg should hold. Size should be a 2^n number
The file to load must be hold only signed integers
"""
def build_msg(size):
    # use global vars
    global data
    global counter

    # init empty msg
    msg = ''

    # build the msg
    if counter < len(data) - 4*size:
        # encode the first number in the msg to 4 bytes signed int
        msg = data[counter:counter+4*size]
        
    elif counter < len(data):
        msg = data[counter::]

    
    counter += size*4
    return msg

"""
Open a server which binds to every interface and listens for a connection on port 1234. Can only handle the first
incomming connection
"""
def connect():
    host = ''  # Symbolic name meaning all available interfaces
    port = 1234  # Arbitrary non-privileged port

    #create and bind the socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))

    # wait for a connection
    s.listen(1)
    print("Waiting for a connection")
    # Connect to the forst incomming connection
    conn, addr = s.accept()
    print('Connected by', addr)
    data_out = []

    #send data to the client
    while True:
        recv_data = conn.recv(1024)      
        msg = build_msg(2**14)
        
        # if we have no more data send a no more data msg to the client      
        if not msg:
            print('no data')
            # wait for a close msg from the client
            conn.sendall(b'end')
            time.sleep(1)
            while True:                              
                close_reply = conn.recv(1024)
                #close the server
                if close_reply == b'close':
                    print('closing')
                    conn.close()
                    break
                else:
                    conn.sendall(b'end')
                    print("end")
                    time.sleep(0.01)
            break
        else:
            # else if we have data send it to the client
            conn.sendall(msg)
                      
            #print('sending {0} bytes'.format(sys.getsizeof(msg)))


def main(fs):
    # load data into memory
    open_file(fs)
    # open the server and start sending data 
    connect()


if __name__ == "__main__":
    #fs_rate = sys.argv[1]
    fs_rate = 48
    main(fs_rate)
